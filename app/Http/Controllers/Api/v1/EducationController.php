<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \Illuminate\Http\Response as Res;
use App\Education;

class EducationController extends BaseController
{
    public function index(){
    	$data = Education::orderBy('created_at','DESC')->get();
    	$this->setStatusCode(Res::HTTP_OK);
        return $this->sendSuccessResponse($data, 'blog list successfully');
    }
}
