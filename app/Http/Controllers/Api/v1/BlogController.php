<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \Illuminate\Http\Response as Res;
use App\Blog;

class BlogController extends BaseController
{
    public function index(){
    	$data = Blog::orderBy('created_at','DESC')->get();
    	$this->setStatusCode(Res::HTTP_OK);
        return $this->sendSuccessResponse($data, 'blog list successfully');
    }
}
