<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Blog;
use App\Education;
use App\Portfolio;
use Illuminate\Support\Carbon;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
        	'name' => 'Admin',
        	'username' => 'admin',
        	'password' => bcrypt('su63d1###')
        ]);
        $faker = Faker::create();
        foreach(range(1,10) as $index)
        {
            DB::table('blogs')->insert([
                'title' => $faker->name,
                'description' => $faker->text,
                'image'=> 'https://cdn.learn2crack.com/wp-content/uploads/2016/02/cover-1024x483.png',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
            DB::table('portfolios')->insert([
                'title' => $faker->name,
                'description' => $faker->text,
                'image'=> 'https://www.notebookcheck.net/fileadmin/Notebooks/News/_nc3/Android_Logo_2.jpg',
                'url'=>'http://www.jineshsubedi.com.np',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
            DB::table('education')->insert([
                'institute' => $faker->name,
                'board' => $faker->name,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }
}
